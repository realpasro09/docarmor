MSBUILD_PATH = "C:/Windows/Microsoft.NET/Framework/v4.0.30319/msbuild.exe"
MSPEC_PATH = "lib/Machine.Specifications.0.8.3/tools/mspec-clr4.exe"
VERSION = ENV["APPVEYOR_BUILD_VERSION"] || "local"
MSTEST_PATH = File.join(ENV['VS110COMNTOOLS'], '..', 'IDE', 'mstest.exe')
BUILD_PATH = File.expand_path('build')
DATABASE_DEPLOYMENT_PATH = File.expand_path('database_deployment')
DEPLOY_PATH = File.expand_path('deploy')
REPORTS_PATH = File.expand_path('testresults')
TEST_RESULTS_PATH = File.expand_path('testresults')
SOLUTION = "SelectSigningDocumentManagement.sln"
SOLUTION_PATH = File.join("src",SOLUTION)
TRXFILE = File.join(REPORTS_PATH, SOLUTION + '.trx')
CONFIG = "Debug"
PATH_7ZIP = "C:/Program Files/7-Zip/7z.exe"
WEB_APP = "SelectSigningDocumentManagement.Web"

task :default => [:all]

task :all => [:build, :createArtifact]

task :build => [:removeArtifacts, :compile]

task :createArtifact do
	puts 'Creating deployment artifact...'
	Dir.mkdir(DEPLOY_PATH) unless Dir.exists?(DEPLOY_PATH)	
	Dir.chdir("build/_publishedWebsites/#{WEB_APP}") do
		sh "\"#{PATH_7ZIP}\" a \"#{DEPLOY_PATH}/#{WEB_APP}-#{VERSION}.zip\" *"
	end
end


task :removeArtifacts do
	require 'fileutils'
	FileUtils.rm_rf BUILD_PATH
	FileUtils.rm_rf REPORTS_PATH
	FileUtils.rm_rf DEPLOY_PATH
	FileUtils.rm_rf DATABASE_DEPLOYMENT_PATH
end
	
task :compile do
	puts 'Compiling solution...'
	sh "#{MSBUILD_PATH} /p:Configuration=#{CONFIG} /p:OutDir=\"#{BUILD_PATH}/\" /p:PostBuildEvent=\"\" #{SOLUTION_PATH}"	
end

task :dbRebuild => [:dbDrop, :dbCreate, :dbSeed]

task :dbDrop => [:compile] do
	Dir.chdir("build/") do
		sh "DatabaseDeployer.exe drop"
	end
end

task :dbCreate => [:compile] do
	Dir.chdir("build/") do
		sh "DatabaseDeployer.exe create"
	end
end

task :dbUpdate => [:compile] do
	Dir.chdir("build/") do
		sh "DatabaseDeployer.exe update"
	end
end

task :dbSeed => [:compile] do
	Dir.chdir("build/") do
		sh "DatabaseDeployer.exe seed"
	end
end
