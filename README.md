SelectSigningDocumentManagement
==========

A SPA starter using Angular, Nancy and NHibernate.


- CQRS-ready with synchronous command dispatcher.
- Domain Events already installed with BlingBag
- SpecFlow and Selenium set up and ready for Features and Acceptance Criteria (scenarios)
- Database deployer re-builds database from domain entities

Active demo: [http://SelectSigningDocumentManagement-starter.azurewebsites.net/](http://SelectSigningDocumentManagement-starter.azurewebsites.net/)


![SelectSigningDocumentManagement](http://fc09.deviantart.net/fs47/i/2009/164/2/8/Decepticon_SelectSigningDocumentManagement_by_davidnery.jpg)
