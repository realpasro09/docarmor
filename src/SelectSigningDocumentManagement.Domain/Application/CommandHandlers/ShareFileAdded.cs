﻿namespace SelectSigningDocumentManagement.Domain.Application.CommandHandlers
{
    public class ShareFileAdded
    {
        public string ShareId { get; protected set; }
        public ShareFileAdded(string shareId)
        {
            ShareId = shareId;
        }
    }
}