﻿using System;
using System.Linq;
using AcklenAvenue.Commands;
using SelectSigningDocumentManagement.Domain.DomainEvents;
using SelectSigningDocumentManagement.Domain.Entities;
using SelectSigningDocumentManagement.Domain.Services;

namespace SelectSigningDocumentManagement.Domain.Application.CommandHandlers
{
    public class ShareFileAdder : ICommandHandler<AddShareFile>
    {
        private readonly IWriteableRepository _writeableRepository;
        private readonly IIdentityGenerator<Guid> _identityGenerator;

        private string GenerateId()
        {
            var i = Guid.NewGuid().ToByteArray().Aggregate<byte, long>(1, (current, b) => current * (b + 1));
            return string.Format("{0:x}", i - DateTime.Now.Ticks);
        }

        public ShareFileAdder(IWriteableRepository writeableRepository, IIdentityGenerator<Guid> identityGenerator)
        {
            _writeableRepository = writeableRepository;
            _identityGenerator = identityGenerator;
        }

        public void Handle(IUserSession userIssuingCommand, AddShareFile command)
        {
            var id = _identityGenerator.Generate();
            var shareId = GenerateId();
            var sharedFile = new Entities.ShareFile(id)
            {
                FileId = command.FileId,
                FileType = command.FileType,
                FileName = command.FileName,
                ShareId = shareId
            };

            _writeableRepository.Create(sharedFile);

            NotifyObservers(new ShareFileAdded(shareId));
        }

        public event DomainEvent NotifyObservers;
    }
}