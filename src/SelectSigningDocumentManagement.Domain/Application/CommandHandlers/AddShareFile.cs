﻿namespace SelectSigningDocumentManagement.Domain.Application.CommandHandlers
{
    public class AddShareFile
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
    }
}