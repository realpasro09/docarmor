using System;
using AcklenAvenue.Commands;
using SelectSigningDocumentManagement.Domain.Services;


namespace SelectSigningDocumentManagement.Domain
{
    public class VisitorSession : IUserSession
    {
        #region IUserSession Members

        public Guid Id { get; private set; }

        #endregion
    }
}