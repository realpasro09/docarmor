using SelectSigningDocumentManagement.Domain.ValueObjects;

namespace SelectSigningDocumentManagement.Domain.Services
{
    public interface IPasswordEncryptor
    {
        EncryptedPassword Encrypt(string clearTextPassword);
    }
}