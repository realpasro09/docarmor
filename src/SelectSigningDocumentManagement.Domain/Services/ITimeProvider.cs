using System;

namespace SelectSigningDocumentManagement.Domain.Services
{
    public interface ITimeProvider
    {
        DateTime Now();
    }
}