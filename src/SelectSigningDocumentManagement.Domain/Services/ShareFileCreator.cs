using System;
using System.Linq;

namespace SelectSigningDocumentManagement.Domain.Services
{
    public class ShareFileCreator : IShareFileCreator
    {
        private readonly IIdentityGenerator<Guid> _identityGenerator;
        private readonly IWriteableRepository _writeableRepository;

        public ShareFileCreator(IIdentityGenerator<Guid> identityGenerator, IWriteableRepository writeableRepository)
        {
            _identityGenerator = identityGenerator;
            _writeableRepository = writeableRepository;
        }

        private string GenerateId()
        {
            var i = Guid.NewGuid().ToByteArray().Aggregate<byte, long>(1, (current, b) => current * (b + 1));
            return string.Format("{0:x}", i - DateTime.Now.Ticks);
        }

        public string ShareFile(string fileId, string name, string type, string message, string password)
        {
            var id = _identityGenerator.Generate();
            var shareId = GenerateId();
            var sharedFile = new Entities.ShareFile(id)
            {
                FileId = fileId,
                FileType = type,
                FileName = name,
                ShareId = shareId,
                Message = message,
                Password = password,
                DateCreated = DateTime.Now.ToLongDateString(),
                SentToSender = false,
                SentToRecipients = 0
            };

            _writeableRepository.Create(sharedFile);

            return shareId;
        }

        public string ShareFile(string fileId, string name, string type, string message, string password, string commonShareId)
        {
            var id = _identityGenerator.Generate();
            var sharedFile = new Entities.ShareFile(id)
            {
                FileId = fileId,
                FileType = type,
                FileName = name,
                ShareId = commonShareId,
                Message = message,
                Password = password,
                DateCreated = DateTime.Now.ToLongDateString(),
                SentToSender = false,
                SentToRecipients = 0,
               
            };

            _writeableRepository.Create(sharedFile);

            return commonShareId;
        }
    }
}