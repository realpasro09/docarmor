using System.Collections.Generic;
using System.Linq;

namespace SelectSigningDocumentManagement.Domain.Services
{
    public class ShareFile : IShareFile
    {
        private readonly IReadOnlyRepository _readOnlyRepository;

        public ShareFile(IReadOnlyRepository readOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
        }

        public List<Entities.ShareFile> GetFromShareId(string shareId)
        {
            var sharedFiles = _readOnlyRepository.Query<Entities.ShareFile>(x => x.ShareId == shareId).OrderBy(x=>x.Id);
            return sharedFiles.ToList();
        }
    }
}