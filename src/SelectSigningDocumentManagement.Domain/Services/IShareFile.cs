using System.Collections.Generic;

namespace SelectSigningDocumentManagement.Domain.Services
{
    public interface IShareFile
    {
        List<Entities.ShareFile> GetFromShareId(string shareId);
    }
}