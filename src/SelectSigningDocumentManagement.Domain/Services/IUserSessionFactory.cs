using SelectSigningDocumentManagement.Domain.Entities;

namespace SelectSigningDocumentManagement.Domain.Services
{
    public interface IUserSessionFactory
    {
        UserLoginSession Create(User executor);
    }
}