namespace SelectSigningDocumentManagement.Domain.Services
{
    public interface IIdentityGenerator<out T>
    {
        T Generate();
    }
}