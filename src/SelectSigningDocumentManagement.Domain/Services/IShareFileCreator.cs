namespace SelectSigningDocumentManagement.Domain.Services
{
    public interface IShareFileCreator
    {
        string ShareFile(string fileId, string name, string type, string message, string password);
        string ShareFile(string fileId, string name, string type, string message, string password, string commonShareId);
    }
}