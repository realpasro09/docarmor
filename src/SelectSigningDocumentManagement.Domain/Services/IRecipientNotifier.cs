namespace SelectSigningDocumentManagement.Domain.Services
{
    public interface IRecipientNotifier
    {
        string CreateRecipientNotifier(string email, string shareId, string sender);
        string GetSenderByRecipientId(string recipientId);
        string GetRecipientEmailByRecipientId(string recipientId);
        void UpdateNotify(string recipientId);
        bool CheckNotification(string recipientId);
    }
}