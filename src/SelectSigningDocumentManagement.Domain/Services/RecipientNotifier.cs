using System;
using System.Linq;
using SelectSigningDocumentManagement.Domain.Entities;

namespace SelectSigningDocumentManagement.Domain.Services
{
    public class RecipientNotifier : IRecipientNotifier
    {
        private string GenerateId()
        {
            var i = Guid.NewGuid().ToByteArray().Aggregate<byte, long>(1, (current, b) => current * (b + 1));
            return string.Format("{0:x}", i - DateTime.Now.Ticks);
        }

        private readonly IIdentityGenerator<Guid> _identityGenerator;
        private readonly IWriteableRepository _writeableRepository;
        private readonly IReadOnlyRepository _readOnlyRepository;

        public RecipientNotifier(IIdentityGenerator<Guid> identityGenerator, 
            IWriteableRepository writeableRepository, IReadOnlyRepository readOnlyRepository)
        {
            _identityGenerator = identityGenerator;
            _writeableRepository = writeableRepository;
            _readOnlyRepository = readOnlyRepository;
        }

        public string CreateRecipientNotifier(string email, string shareId, string sender)
        {
            var id = _identityGenerator.Generate();
            var recipientId = GenerateId();
            var recipientNotification = new RecipientNotification(id)
            {
                SharedId = shareId,
                RecipientId = recipientId,
                Email = email,
                Sender = sender,
                Status = false
            };

            _writeableRepository.Create(recipientNotification);


            var shareFile = _readOnlyRepository.Query<Entities.ShareFile>(x => x.ShareId == shareId).ToList();
            foreach (var file in shareFile)
            {
                file.SentToRecipients += 1;
                _writeableRepository.Update(file);    
            }
            
            return recipientId;
        }

        public string GetSenderByRecipientId(string recipientId)
        {
            return _readOnlyRepository.First<RecipientNotification>(x => x.RecipientId == recipientId).Sender;
        }

        public string GetRecipientEmailByRecipientId(string recipientId)
        {
            return _readOnlyRepository.First<RecipientNotification>(x => x.RecipientId == recipientId).Email;
        }

        public void UpdateNotify(string recipientId)
        {
            var updateNotification =
                _readOnlyRepository.Query<RecipientNotification>(x => x.RecipientId == recipientId).First();

            updateNotification.Status = true;
            _writeableRepository.Update(updateNotification);
        }

        public bool CheckNotification(string recipientId)
        {
            return _readOnlyRepository.First<RecipientNotification>(x => x.RecipientId == recipientId).Status;
        }
    }
}