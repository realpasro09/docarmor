using System;

namespace SelectSigningDocumentManagement.Domain
{
    public interface IEntity
    {
        Guid Id { get; }
    }
}