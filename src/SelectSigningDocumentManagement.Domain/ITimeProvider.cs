using System;

namespace SelectSigningDocumentManagement.Domain
{
    public interface ITimeProvider
    {
        DateTime Now();
    }
}