namespace SelectSigningDocumentManagement.Domain
{
    public interface IUserSessionFactory
    {
        UserSession Create(User executor);
    }
}