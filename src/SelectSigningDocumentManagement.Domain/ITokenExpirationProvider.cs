using System;

namespace SelectSigningDocumentManagement.Domain
{
    public interface ITokenExpirationProvider
    {
        DateTime GetExpiration(DateTime now);
    }
}