﻿namespace SelectSigningDocumentManagement.Domain
{
    public enum ValidationFailureType
    {
        Missing,
        DoesNotExist,
        Expired
    }
}