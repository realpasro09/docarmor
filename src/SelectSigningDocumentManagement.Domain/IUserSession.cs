using System;

namespace SelectSigningDocumentManagement.Domain
{
    public interface IUserSession
    {
        Guid Id { get; }
    }
}