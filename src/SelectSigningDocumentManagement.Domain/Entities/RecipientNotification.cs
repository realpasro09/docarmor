using System;

namespace SelectSigningDocumentManagement.Domain.Entities
{
    public class RecipientNotification: IEntity
    {
        public RecipientNotification(Guid id)
        {
            Id = id;
        }

        protected RecipientNotification()
        {

        }

        public virtual Guid Id { get; protected set; }
        public virtual string SharedId { get; set; }
        public virtual string RecipientId { get; set; }
        public virtual string Email { get; set; }
        public virtual string Sender { get; set; }
        public virtual bool Status { get; set; }
    }
}