﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectSigningDocumentManagement.Domain.Entities
{
    public class Pricing : Entity
    {
        public Pricing(Guid id)
        {
            Id = id;
        }

        protected Pricing()
        {

        }

        public virtual Guid Id { get; set; }

        public virtual string FeeCode{ get; set; }

        public virtual string FeeDescription { get; set; }
        
        public virtual string Amount { get; set; }

        public virtual string ClientID { get; set; }

    }
}
