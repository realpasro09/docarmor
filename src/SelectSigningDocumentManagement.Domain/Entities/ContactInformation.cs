using System;

namespace SelectSigningDocumentManagement.Domain.Entities
{
    public class ContactInformation : Entity
    {
        public ContactInformation(Guid id)
        {
            Id = id;
        }

        protected ContactInformation()
        {

        }

        #region Contact Information

        public virtual Guid Id { get; set; }

        public virtual string ClientId { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string Title { get; set; }

        public virtual string Office { get; set; }

        public virtual string Ext { get; set; }

        public virtual string DirectLine { get; set; }

        public virtual string Mobile { get; set; }

        public virtual string Email { get; set; }

        public virtual string OnlinePassword { get; set; }

        public virtual string ServiceAgreementName { get; set; }

        public virtual string ServiceAgreementNameFileId { get; set; }

        #endregion

        #region Branch Address

        public virtual string CompanyBranch { get; set; }

        public virtual string StreetBranch { get; set; }

        public virtual string CityBranch { get; set; }

        public virtual string StateBranch { get; set; }

        public virtual string ZipCodeBranch { get; set; }

        #endregion

        #region ShippingAddress

        public virtual string CompanyShipping { get; set; }

        public virtual string StreetShipping { get; set; }

        public virtual string CityShipping { get; set; }

        public virtual string StateShipping { get; set; }

        public virtual string ZipCodeShipping { get; set; }

        public virtual string Attention { get; set; }

        #endregion

        #region Shipping Info

        public virtual string Courier { get; set; }

        public virtual string AccountNumber { get; set; }
        
        #endregion

        #region Special Instructions
        
        public virtual string InternalNotes { get; set; }

        public virtual string SigningInstructions { get; set; }
                
        #endregion

    }
}