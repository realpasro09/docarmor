using System;

namespace SelectSigningDocumentManagement.Domain.Entities
{
    public class ShareFile : IEntity
    {
        public ShareFile(Guid id)
        {
            Id = id;
        }

        protected ShareFile()
        {
            
        }

        public virtual Guid Id { get; protected set; }
        public virtual string FileId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FileType { get; set; }
        public virtual string ShareId { get; set; }
        public virtual string Message { get; set; }
        public virtual string Password { get; set; }
        public virtual string Phone { get; set; }
        public virtual bool SentToSender { get; set; }
        public virtual int SentToRecipients { get; set; }
        public virtual string DateCreated { get; set; }
        public virtual string ShareName { get; set; }
    }
}