namespace SelectSigningDocumentManagement.Domain
{
    public interface ITokenGenerator<out T>
    {
        T Generate();
    }
}