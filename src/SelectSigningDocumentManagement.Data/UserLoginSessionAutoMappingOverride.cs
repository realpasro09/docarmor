using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using SelectSigningDocumentManagement.Domain.Entities;

namespace SelectSigningDocumentManagement.Data
{
    public class UserLoginSessionAutoMappingOverride : IAutoMappingOverride<UserLoginSession>
    {
        public void Override(AutoMapping<UserLoginSession> mapping)
        {            
        }
    }
}