using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using SelectSigningDocumentManagement.Domain.Entities;

namespace SelectSigningDocumentManagement.Data
{
    public class UserAutoMappingOverride : IAutoMappingOverride<User>
    {
        public void Override(AutoMapping<User> mapping)
        {
            mapping.HasManyToMany<Role>(x => x.UserRoles).Cascade.All().Table("UsersRoles");
            mapping.HasManyToMany<UserAbility>(x => x.UserAbilities).Cascade.All().Table("UserAbilities");
        }
        
    }
    public class ShareFileAutoMappingOverride : IAutoMappingOverride<ShareFile>
    {
        public void Override(AutoMapping<User> mapping)
        {
            mapping.HasManyToMany<Role>(x => x.UserRoles).Cascade.All().Table("UsersRoles");
            mapping.HasManyToMany<UserAbility>(x => x.UserAbilities).Cascade.All().Table("UserAbilities");
        }

        public void Override(AutoMapping<ShareFile> mapping)
        {
            mapping.Map(x => x.Message).Length(4000);
        }
    }
}