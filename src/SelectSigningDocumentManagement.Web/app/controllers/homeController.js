﻿angular.module('SelectSigningDocumentManagement.Controllers').controller('homeController', 
    ['$scope', 'userService', '$location', 'adminService',
        'Upload', 'cfpLoadingBar', 'toastr', 'blockUI',
    function ($scope, userService, $location, adminService,
        Upload, cfpLoadingBar, toastr, blockUI) {
        $scope.link = "";
        $scope.showLink = false;
        $scope.progress = 0;
        $scope.showfile1 = false;
        $scope.showfile2 = false;
        $scope.showfile3 = false;
        $scope.showfile4 = false;
        $scope.showfile5 = false;
        $scope.maxFiles = false;
        $scope.allFilesTransfered = false;
        $scope.share = {
            link: false
        }
        function validEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var result = re.test(email);
            return result;
        }

        $scope.existingDocuments = [];
        adminService.DeleteFiles();
        $scope.addFile = function() {
            if (!$scope.showfile1) {
                $("#fileinput1").click();
            } else if (!$scope.showfile2) {
                $("#fileinput2").click();
            } else if (!$scope.showfile3) {
                $("#fileinput3").click();
            } else if (!$scope.showfile4) {
                $("#fileinput4").click();
            } else if (!$scope.showfile5) {
                $("#fileinput5").click();
            }
        }

        $scope.removeFile = function () {
            $scope.maxFiles = false;
            
            var searchTerm = "";
            if ($scope.showfile5) {
                $scope.fileData5 = null;
                $scope.showfile5 = false;
                searchTerm = $scope.fileName5;
                $scope.fileName5 = null;
            } else if ($scope.showfile4) {
                $scope.fileData4 = null;
                $scope.showfile4 = false;
                searchTerm = $scope.fileName4;
                $scope.fileName4 = null;
            } else if ($scope.showfile3) {
                $scope.fileData3 = null;
                $scope.showfile3 = false;
                searchTerm = $scope.fileName3;
                $scope.fileName3 = null;
            } else if ($scope.showfile2) {
                $scope.fileData2 = null;
                $scope.showfile2 = false;
                searchTerm = $scope.fileName2;
                $scope.fileName2 = null;
            } else if ($scope.showfile1) {
                $scope.fileData1 = null;
                $scope.showfile1 = false;
                searchTerm = $scope.fileName1;
                $scope.fileName1 = null;
            }
            for (var i = $scope.existingDocuments.length - 1; i >= 0; i--) {
                if ($scope.existingDocuments[i] === searchTerm) {
                    $scope.existingDocuments.splice(i, 1);
                    break;
                }
            }
        }

        var checkIfFileExist = function (fileUrl) {
            var exist = false;
            for (var i = 0; i < $scope.existingDocuments.length; i++) {
                if (fileUrl === $scope.existingDocuments[i]) {
                    exist = true;
                }
            }
            if (!exist) {
                $scope.existingDocuments.push(fileUrl);
            }
            return exist;
        }

        $scope.$watch("fileData1", function (newValue) {
            if (newValue && newValue[0] && !$scope.showfile1) {
                $scope.existingDocuments.push(newValue[0].name);
                $scope.showfile1 = true;
                $scope.fileName1 = newValue[0].name;
            }
            if (newValue && newValue[1]) {
                $scope.fileData2 = [newValue[1]];
            }
            if (newValue && newValue[2]) {
                $scope.fileData3 = [newValue[2]];
            }
            if (newValue && newValue[3]) {
                $scope.fileData4 = [newValue[3]];
            }
            if (newValue && newValue[4]) {
                $scope.fileData5 = [newValue[4]];
            }
        });

        $scope.$watch("fileData2", function (newValue) {
            if (newValue && newValue[0] && !$scope.showfile2) {
                $scope.showfile2 = !checkIfFileExist(newValue[0].name);
                $scope.fileName2 = newValue[0].name;
            }
            if (newValue && newValue[1]) {
                if ($scope.showfile2) {
                    $scope.fileData3 = [newValue[1]];
                } else {
                    $scope.fileData2 = [newValue[1]];
                }
            }
            if (newValue && newValue[2]) {
                if ($scope.showfile2) {
                    if ($scope.showfile3) {
                        $scope.fileData4 = [newValue[2]];
                    } else {
                        $scope.fileData3 = [newValue[2]];
                    }
                } else {
                    $scope.fileData2 = [newValue[2]];
                }
            }
            if (newValue && newValue[3]) {
                if ($scope.showfile2) {
                    if ($scope.showfile3) {
                        if ($scope.showfile4) {
                            $scope.fileData5 = [newValue[2]];
                        } else {
                            $scope.fileData4 = [newValue[2]];
                        }
                    } else {
                        $scope.fileData3 = [newValue[2]];
                    }
                } else {
                    $scope.fileData2 = [newValue[3]];
                }
            }
        });

        $scope.$watch("fileData3", function (newValue) {
            if (newValue && newValue[0] && !$scope.showfile3) {
                $scope.showfile3 = !checkIfFileExist(newValue[0].name);
                $scope.fileName3 = newValue[0].name;
            }
            if (newValue && newValue[1]) {
                if ($scope.showfile3) {
                    $scope.fileData4 = [newValue[1]];
                } else {
                    $scope.fileData3 = [newValue[1]];
                }
            }
            if (newValue && newValue[2]) {
                if ($scope.showfile3) {
                    if ($scope.showfile4) {
                        $scope.fileData5 = [newValue[2]];
                    } else {
                        $scope.fileData4 = [newValue[2]];
                    }
                } else {
                    $scope.fileData3 = [newValue[2]];
                }
                
            }
        });

        $scope.$watch("fileData4", function (newValue) {
            if (newValue && newValue[0] && !$scope.showfile4) {
                $scope.showfile4 = !checkIfFileExist(newValue[0].name);
                $scope.fileName4 = newValue[0].name;
            }
            if (newValue && newValue[1]) {
                if ($scope.showfile4) {
                    $scope.fileData5 = [newValue[1]];
                } else {
                    $scope.fileData4 = [newValue[1]];
                }
                
            }
        });

        $scope.$watch("fileData5", function (newValue) {
            if (newValue && newValue[0] && !$scope.showfile5) {
                $scope.showfile5 = !checkIfFileExist(newValue[0].name);
                $scope.fileName5 = newValue[0].name;
                $scope.maxFiles = $scope.showfile5;
            }
        });

        var getSmsData = function (shareId) {
            var smsData = {
                ShareId: shareId,
                PhoneNumberEmail: $scope.recipientMobile,
                Password: $scope.password,
                FileName: $scope.namethefile
            }
            return smsData;
        }

        var getSendEmailData = function (shareLink) {
            var emails = [];
            emails.push($scope.email1);
            if ($scope.email2 !== undefined) {
                emails.push($scope.email2);
            }
            if ($scope.email3 !== undefined) {
                emails.push($scope.email3);
            }
            if ($scope.email4 !== undefined) {
                emails.push($scope.email4);
            }
            if ($scope.email5 !== undefined) {
                emails.push($scope.email5);
            }
            return {
                ShareLink: shareLink,
                EmailSender: $scope.sender,
                Emails: emails,
                Message: $scope.namethefile + '_' + $scope.message
            }
        }

        var share = function (shareId) {
            if ($scope.share.link) {
                $scope.shareLink = "https://" + $location.host() + "/#/file/" + shareId;
                if ($scope.recipientMobile) {
                    adminService.SendSms(getSmsData(shareId)).then(function () {

                    });
                };
            } else {
                adminService.SendMail(getSendEmailData("https://" + $location.host() + "/#/file/" + shareId)).then(function () {
                    if ($scope.recipientMobile) {
                        adminService.SendSms(getSmsData(shareId)).then(function () {

                        });
                    };
                }).catch(function() {
                    toast.error("there was an error trying to send an email to the recipients or sender");
                });
            }
            $scope.sender = null;
            $scope.email1 = null;
            $scope.email2 = null;
            $scope.email3 = null;
            $scope.email4 = null;
            $scope.email5 = null;
        };
        var uploadFile5 = function (shareId) {
            var file = $scope.fileData5[0];
            Upload.upload({
                url: '/fileupload',
                method: 'POST',
                fields: { 'commonShareId': shareId, 'message': $scope.namethefile + '_' + $scope.message, 'password': $scope.password },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                blockUI.message("Uploading and Encrypting File: " + $scope.fileName5 + " Progress: " + progressPercentage + "%");
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            }).success(function (data, status, headers, config) {
                blockUI.stop();
                cfpLoadingBar.complete();
                $scope.allFilesTransfered = true;
                share(shareId);
            }).error(function (data, status, headers, config) {
                blockUI.stop();
                cfpLoadingBar.complete();
                toastr.error('Error trying to upload file: ' + $scope.fileName5);
                toastr.error('Please contact service provider and share this error: ' + status);
            });
        };

        var uploadFile4 = function (shareId) {
            var file = $scope.fileData4[0];
            Upload.upload({
                url: '/fileupload',
                method: 'POST',
                fields: { 'commonShareId': shareId, 'message': $scope.namethefile + '_' + $scope.message, 'password': $scope.password },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                blockUI.message("Uploading and Encrypting File: " + $scope.fileName4 + " Progress: " + progressPercentage + "%");
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            }).success(function (data, status, headers, config) {
                if ($scope.showfile5) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName5);
                    uploadFile5(shareId);
                } else {
                    blockUI.stop();
                    $scope.allFilesTransfered = true;
                    cfpLoadingBar.complete();
                    share(shareId);
                }
            }).error(function (data, status, headers, config) {
                blockUI.stop();
                cfpLoadingBar.complete();
                toastr.error('Error trying to upload file: ' + $scope.fileName4);
                toastr.error('Please contact service provider and share this error: ' + status);
                if ($scope.showfile5) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName5);
                    uploadFile5(shareId);
                }
            });
        };

        var uploadFile3 = function (shareId) {
            var file = $scope.fileData3[0];
            Upload.upload({
                url: '/fileupload',
                method: 'POST',
                fields: { 'commonShareId': shareId, 'message': $scope.namethefile + '_' + $scope.message, 'password': $scope.password },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                blockUI.message("Uploading and Encrypting File: " + $scope.fileName3 + " Progress: " + progressPercentage + "%");
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            }).success(function (data, status, headers, config) {
                if ($scope.showfile4) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName4);
                    uploadFile4(shareId);
                } else {
                    blockUI.stop();
                    $scope.allFilesTransfered = true;
                    cfpLoadingBar.complete();
                    share(shareId);
                }
            }).error(function (data, status, headers, config) {
                blockUI.stop();
                cfpLoadingBar.complete();
                toastr.error('Error trying to upload file: ' + $scope.fileName3);
                toastr.error('Please contact service provider and share this error: ' + status);
                if ($scope.showfile4) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName4);
                    uploadFile4(shareId);
                }
            });
        };

        var uploadFile2 = function (shareId) {
            var file = $scope.fileData2[0];
            Upload.upload({
                url: '/fileupload',
                method: 'POST',
                fields: { 'commonShareId': shareId, 'message': $scope.namethefile + '_' + $scope.message, 'password': $scope.password },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                blockUI.message("Uploading and Encrypting File: " + $scope.fileName2 + " Progress: " + progressPercentage + "%");
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            }).success(function (data, status, headers, config) {
                if ($scope.showfile3) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName3);
                    uploadFile3(shareId);
                } else {
                    blockUI.stop();
                    $scope.allFilesTransfered = true;
                    cfpLoadingBar.complete();
                    share(shareId);
                }
            }).error(function (data, status, headers, config) {
                blockUI.stop();
                cfpLoadingBar.complete();
                toastr.error('Error trying to upload file: ' + $scope.fileName2);
                toastr.error('Please contact service provider and share this error: ' + status);
                if ($scope.showfile3) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName3);
                    uploadFile3(shareId);
                }
            });
        };

        var uploadFile1 = function() {
            var file = $scope.fileData1[0];
            Upload.upload({
                url: '/fileupload',
                method: 'POST',
                fields: { 'message': $scope.namethefile + '_' + $scope.message, 'password': $scope.password },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                blockUI.message("Uploading and Encrypting File: " + $scope.fileName1 + " Progress: " + progressPercentage + "%");
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            }).success(function (shareId, status, headers, config) {
                if ($scope.showfile2) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName2);
                    uploadFile2(shareId);
                } else {
                    blockUI.stop();
                    $scope.allFilesTransfered = true;
                    cfpLoadingBar.complete();
                    share(shareId);
                }
            }).error(function (data, status, headers, config) {
                blockUI.stop();
                cfpLoadingBar.complete();
                toastr.error('Error trying to upload file: ' + $scope.fileName1);
                toastr.error('Please contact service provider and share this error: ' + status);
                if ($scope.showfile2) {
                    blockUI.message("Uploading and Encrypting File: " + $scope.fileName2);
                    uploadFile2(shareId);
                }
            });
        };
        
        $scope.newTransfer = function() {
            $scope.link = "";
            $scope.showLink = false;
            $scope.progress = 0;
            $scope.message = "";
            $scope.password = "";
            $scope.namethefile = "";
            $scope.showfile1 = false;
            $scope.showfile2 = false;
            $scope.showfile3 = false;
            $scope.showfile4 = false;
            $scope.showfile5 = false;
            $scope.maxFiles = false;
            $scope.recipientMobile = "";
            $scope.allFilesTransfered = false;
            $scope.carrierDomain = undefined;
            $scope.shareLink = undefined;
            $scope.share = {
                link: false
            }
            $scope.existingDocuments = [];
        };

        var startProcess = function() {
            cfpLoadingBar.start();
            blockUI.start("Uploading and Encrypting File: " + $scope.fileName1);
            $scope.showLink = false;
            uploadFile1();
        }
        $scope.upload = function () {
            if ($scope.share.link) {
                if ($scope.namethefile === undefined || $scope.namethefile === '') {
                    toastr.warning("You need to provide a file name");
                    document.getElementById("namethefile").focus();
                } else if ($scope.recipientMobile === undefined || $scope.recipientMobile === '') {
                    toastr.warning("You need to provide a recipient mobile");
                    document.getElementById("recipientMobile").focus();
                } else if ($scope.password === undefined || $scope.password === '') {
                    toastr.warning("You need to provide a password");
                    document.getElementById("mypassword").focus();
                } else {
                    startProcess();
                }
                    
                
            } else {
                if ($scope.sender === undefined || $scope.sender === "") {
                    toastr.warning("You need to include the sender");
                    document.getElementById("sender").focus();
                } else if (!validEmail($scope.sender)) {
                    toastr.warning("Sender Email is not valid");
                    document.getElementById("sender").focus();
                } else if (($scope.email1 === undefined || $scope.email1 === "") &&
                    ($scope.email2 === undefined || $scope.email2 === "") &&
                    ($scope.email3 === undefined || $scope.email3 === "") &&
                    ($scope.email4 === undefined || $scope.email4 === "") &&
                    ($scope.email5 === undefined || $scope.email5 === "")) {
                    toastr.warning("You need to include at least one recipient");
                    document.getElementById("email1").focus();
                } else if ($scope.email1 !== undefined && $scope.email1 !== "" && $scope.email1 !== null && !validEmail($scope.email1)) {
                    toastr.warning("Recipient Email #1 is not valid");
                    document.getElementById("email1").focus();
                } else if ($scope.email2 !== undefined && $scope.email2 !== "" && $scope.email2 !== null && !validEmail($scope.email2)) {
                    toastr.warning("Recipient Email #2 is not valid");
                    document.getElementById("email2").focus();
                } else if ($scope.email3 !== undefined && $scope.email3 !== "" && $scope.email3 !== null && !validEmail($scope.email3)) {
                    toastr.warning("Recipient Email #3 is not valid");
                    document.getElementById("email3").focus();
                } else if ($scope.email4 !== undefined && $scope.email4 !== "" && $scope.email4 !== null && !validEmail($scope.email4)) {
                    toastr.warning("Recipient Email #4 is not valid");
                    document.getElementById("email4").focus();
                } else if ($scope.email5 !== undefined && $scope.email5 !== "" && $scope.email5 !== null && !validEmail($scope.email5)) {
                    toastr.warning("Recipient Email #5 is not valid");
                    document.getElementById("email5").focus();
                } else if ($scope.namethefile === undefined || $scope.namethefile === "") {
                    toastr.warning("You need to provide a file name");
                    document.getElementById("namethefile").focus();
                } else if ($scope.recipientMobile === undefined || $scope.recipientMobile === "") {
                    toastr.warning("You need to provide a recipient mobile");
                    document.getElementById("recipientMobile").focus();
                } else if ($scope.password === undefined || $scope.password === "") {
                    toastr.warning("You need to provide a password");
                    document.getElementById("mypassword").focus();
                } else {
                    startProcess();
                }
            }
            
        };
        $scope.copy = function () {
            toastr.success('Link Copied!');
        };
        
    //var user = userService.GetUser();
    //if (!user) {
    //    $location.path("/login");
    //} 
}]);