﻿angular.module('SelectSigningDocumentManagement.Controllers').controller('escrowController',
    function ($scope, $location, clientService, toastr) {

        $scope.$on('$viewContentLoaded', function () {
            $('#example2').dataTable({ searching: false, ordering: false });
        });

        var getClients = function() {
            clientService.GetAllClients().then(function (clients) {
                $scope.clients = clients;
            });
        }

        getClients();

        $scope.deleteClient = function (client) {
            var payload = {
                ClientId: client.id
            };

            clientService.DeleteClient(payload).then(function(result) {
                if (result === "OK") {
                    toastr.success("Client deleted successfully.");
                    getClients();
                }
            });
        }
    });