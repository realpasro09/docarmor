﻿angular.module('SelectSigningDocumentManagement.Controllers').controller('escrowNewClientController',
    function ($scope, $location, clientService, toastr, Upload, blockUI, cfpLoadingBar) {
        $scope.tog = 1;
        $scope.feeDescription = "het";
        $scope.feeAmount = "";
        $scope.inputType = "password";
        $scope.pricingsData = [
        { 'FeeCode': 'SSET', 'FeeDescription': 'Single Set Overnight Signing', 'Amount': '150', 'id': '0' },
          { 'FeeCode': 'DSET', 'FeeDescription': 'Double Set Overnight Signing', 'Amount': '225', 'id': '1' },
          { 'FeeCode': 'SEDOC', 'FeeDescription': 'Single Set Email Signing', 'Amount': '200', 'id': '2' },
          { 'FeeCode': 'DEDOC', 'FeeDescription': 'Double Set Email Signing', 'Amount': '350', 'id': '3' },
          { 'FeeCode': 'CAN', 'FeeDescription': 'Cancellation', 'Amount': '0', 'id': '4' },
          { 'FeeCode': 'REF', 'FeeDescription': 'Refused to Sign', 'Amount': '50', 'id': '5' },
          { 'FeeCode': 'DROP', 'FeeDescription': 'Courier Fee to Drop Off Docs', 'Amount': '30', 'id': '6' },
          { 'FeeCode': 'PICK', 'FeeDescription': 'Courier Fee to Pick Up Docs', 'Amount': '30', 'id': '7' },
          { 'FeeCode': 'PUDO', 'FeeDescription': 'Courier Fee to Pick Up and Drop Docs', 'Amount': '60', 'id': '8' },
        { 'FeeCode': 'PRINT', 'FeeDescription': 'Print Fee Only', 'Amount': '25', 'id': '9' },
        { 'FeeCode': 'MISC', 'FeeDescription': 'Misc. Fee', 'Amount': '0', 'id': '10' },
        { 'FeeCode': 'TRIP', 'FeeDescription': 'Notary Made Trip', 'Amount': '50', 'id': '11' }
        ];

        $scope.editingData = [];

        for (var i = 0, length = $scope.pricingsData.length; i < length; i++) {
            $scope.editingData[$scope.pricingsData[i].id] = false;
        }


        $scope.modify = function (tableData) {
            $scope.editingData[tableData.id] = true;
        };


        $scope.update = function (tableData) {
            $scope.editingData[tableData.id] = false;
        };

        $scope.hideShowPassword = function () {
            if ($scope.inputType == 'password')
                $scope.inputType = 'text';
            else
                $scope.inputType = 'password';
        };
        var previousElement;
        $scope.$on('$viewContentLoaded', function () {
            var table = $('#example2').dataTable();
            $(".js-source-states").select2();
            $(".js-source-states1").select2();
            $('#example4').dataTable({ paging: false, searching: false, ordering: false });
            // $('#example3').dataTable({ paging: false, searching: false, ordering: false });
            //$('#example3 tbody').on('click', 'tr', function () {
            //    if ($(this).hasClass('selected')) {
            //        $(this).removeClass('selected');
            //    }
            //    else {
            //        table.$('tr.selected').removeClass('selected');
            //        $(this).addClass('selected');
            //        $scope.feeDescription = $(this)[0].childNodes[3].innerText;
            //        $scope.feeAmount = $(this)[0].childNodes[5].innerText;
            //        $scope.$apply();
            //        if (previousElement && previousElement.hasClass('selected')) {
            //            previousElement.removeClass('selected');
            //        }
            //        previousElement = $(this);
            //    }
            //});


            $('#button').click(function () {
                table.row('.selected').remove().draw(false);
            });
        });

        clientService.GetContactInformationShortId().then(function (shortId) {
            $scope.clientId = shortId;
            $("#firstName").focus();
        });

        $scope.saveClient = function () {
            var client = {
                contactInformation: {
                    clientId: $scope.clientId,
                    firstName: $scope.firstName,
                    lastName: $scope.lastName,
                    title: $scope.clientTitle,
                    office: $scope.office,
                    ext: $scope.ext,
                    directLine: $scope.directLine,
                    mobile: $scope.mobile,
                    email: $scope.email,
                    onlinePassword: $scope.onlinePassword,
                    companybranch: $scope.companybranch,
                    streetbranch: $scope.streetbranch,
                    citybranch: $scope.citybranch,
                    statebranch: $scope.statebranch,
                    zipcodebranch: $scope.zipcodebranch,
                    companyshipping: $scope.companyshipping,
                    streetshipping: $scope.streetshipping,
                    cityshipping: $scope.cityshipping,
                    stateshipping: $scope.stateshipping,
                    zipcodeshipping: $scope.zipcodeshipping,
                    attention: $scope.attention,
                    courier: $scope.courier,
                    accountnumber: $scope.accountnumber,
                    internalnotes: $scope.internalnotes,
                    signinginstructions: $scope.signinginstructions

                }
            };
            clientService.NewClient(client).then(function (result) {
                if (result === "OK") {
                    if ($scope.fileData1) {
                        var file = $scope.fileData1[0];

                        cfpLoadingBar.start();
                        blockUI.message("Uploading " + $scope.fileName1);

                        Upload.upload({
                            url: "/serviceAgreementUpload",
                            method: "POST",
                            fields: { 'clientId': $scope.clientId },
                            file: file
                        }).success(function (uploadResult) {
                            blockUI.stop();
                            cfpLoadingBar.complete();
                            if (uploadResult === "OK") {
                                $scope.AddClientPricings();
                                $scope.clearClient();
                                $location.path("/escrow");
                            } else {
                                toastr.error("There was an error trying to upload the service agreement");
                            }
                        });

                    } else {
                        $scope.AddClientPricings();
                        blockUI.stop();
                        cfpLoadingBar.complete();
                        $scope.clearClient();
                        $location.path("/escrow");
                    }

                } else {
                    toastr.error("There was an error trying to save the client");
                }
            });
        };

        $scope.AddClientPricings = function () {
            for (var k in $scope.pricingsData) {
                var pricingRequest = {
                    FeeCode: $scope.pricingsData[k].FeeCode,
                    FeeDescription: $scope.pricingsData[k].FeeDescription,
                    Amount: $scope.pricingsData[k].Amount,
                    clientId: $scope.clientId
                };
                var clientpricing = {
                    pricingInformation: pricingRequest
                };
                clientService.NewClientPricings(clientpricing).then(function (result) {
                    if (result === "OK") {
                    }
                    else {
                        toastr.error("There was an error trying to save the pricing");
                    }
                });
            }
        };
        $scope.clearClient = function () {

            $scope.firstName = "";
            $scope.lastName = "";
            $scope.clientTitle = "";
            $scope.office = "";
            $scope.ext = "";
            $scope.directLine = "";
            $scope.mobile = "";
            $scope.email = "";
            $scope.onlinePassword = "";

            $scope.fileName1 = null;

            $scope.companybranch = "";
            $scope.streetbranch = "";
            $scope.citybranch = "";
            $scope.zipcodebranch = "";

            $scope.companyshipping = "";
            $scope.streetshipping = "";
            $scope.cityshipping = "";
            $scope.zipcodeshipping = "";
            $scope.attention = "";

            $scope.courier = "";
            $scope.accountnumber = "";

            $scope.internalnotes = "";
            $scope.signinginstructions = "";
        };

        $scope.copyaddress = function () {

            if ($scope.confirmed) {

                $scope.companyshipping = $scope.companybranch;
                $scope.streetshipping = $scope.streetbranch;
                $scope.cityshipping = $scope.citybranch;
                $scope.zipcodeshipping = $scope.zipcodebranch;
                $scope.stateshipping = $scope.statebranch;
                var $selectStateShipping = $(".js-source-states1");
                $selectStateShipping.val($scope.statebranch).trigger("change");

            }
            else {
                $scope.confirmed = false;
            }

        };

        $scope.addServiceAgreement = function () {
            $("#fileinput1").click();
        };
        $scope.$watch("fileData1", function (newValue) {
            if (newValue && newValue[0]) {
                $scope.fileName1 = newValue[0].name;
            }
        });
    });