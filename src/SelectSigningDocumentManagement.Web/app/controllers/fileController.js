﻿angular.module('SelectSigningDocumentManagement.Controllers').controller('fileController',
    function($scope, $location, $routeParams, adminService, toastr) {

        $scope.shareId = $routeParams.fileId.split('-')[0];
        $scope.recipientId = $routeParams.fileId.split('-')[1];

        $scope.fileId1 = null;
        $scope.fileId2 = null;
        $scope.fileId3 = null;
        $scope.fileId4 = null;
        $scope.fileId5 = null;
        $scope.fileName1 = null;
        $scope.fileName2 = null;
        $scope.fileName3 = null;
        $scope.fileName4 = null;
        $scope.fileName5 = null;
        $scope.type1 = null;
        $scope.type2 = null;
        $scope.type3 = null;
        $scope.type4 = null;
        $scope.type5 = null;


        adminService.GetSharedInfo($scope.shareId).then(function(filesInfo) {
            if (filesInfo[0]) {
                var message = filesInfo[0].message.split('_');
                $scope.fileId1 = filesInfo[0].fileId;
                $scope.fileName1 = filesInfo[0].fileName;
                $scope.type1 = filesInfo[0].fileType;
                $scope.message = (message[1] === undefined) ? '' : message[1];
                $scope.mypassword = filesInfo[0].password;
            }
            if (filesInfo[1]) {
                $scope.fileId2 = filesInfo[1].fileId;
                $scope.fileName2 = filesInfo[1].fileName;
                $scope.type2 = filesInfo[1].fileType;
            }
            if (filesInfo[2]) {
                $scope.fileId3 = filesInfo[2].fileId;
                $scope.fileName3 = filesInfo[2].fileName;
                $scope.type3 = filesInfo[2].fileType;
            }
            if (filesInfo[3]) {
                $scope.fileId4 = filesInfo[3].fileId;
                $scope.fileName4 = filesInfo[3].fileName;
                $scope.type4 = filesInfo[3].fileType;
            }
            if (filesInfo[4]) {
                $scope.fileId5 = filesInfo[4].fileId;
                $scope.fileName5 = filesInfo[4].fileName;
                $scope.type5 = filesInfo[4].fileType;
            }
        }).catch(function(error) {
            console.log(error);
        });

        $('#myModal').on('shown.bs.modal', function() {
            $('#mypassword').focus();
        }).on('hide.bs.modal', function(event) {

            if ($scope.password === $scope.mypassword) {

                if ($scope.click === "1") {
                    $scope.downloadFile1();
                } else if ($scope.click === "2") {
                    $scope.downloadFile2();
                } else if ($scope.click === "3") {
                    $scope.downloadFile3();
                } else if ($scope.click === "4") {
                    $scope.downloadFile4();
                } else if ($scope.click === "5") {
                    $scope.downloadFile5();
                }
            } else {
                toastr.error("Password is not correct");
            }
            $scope.password = "";
        });
        $('#myForgotModal').on('shown.bs.modal', function() {
            $('#phone').focus();
        }).on('hide.bs.modal', function(event) {

            //if ($scope.password === $scope.mypassword) {
            var payload = {
                ShareId: $scope.shareId,
                RecipientId: $scope.recipientId,
                PhoneToVerify: $scope.phoneNumber,
                EmailToVerify: $scope.email
            };
            adminService.VerifyPassword(payload).then(function (sharefile) {
                if (sharefile === null || sharefile === undefined || sharefile === "") {
                    toastr.error("Your phone number and email are not valid.");
                } else {
                    var smsData = {
                        ShareId: $scope.shareId,
                        PhoneNumberEmail: sharefile.phone,
                        Password: sharefile.password,
                        FileName: sharefile.shareName
                    }
                    adminService.SendSmsRetrievingPassword(smsData).then(function () {
                        toastr.success("you will receive a sms with the password.");
                    });
                }
                
            });
            
            //}
            $scope.phoneNumber = "";
            $scope.email = "";
        });

        $scope.file1Click = function() {
            $scope.click = "1";
        };
        $scope.file2Click = function() {
            $scope.click = "2";
        };
        $scope.file3Click = function() {
            $scope.click = "3";
        };
        $scope.file4Click = function() {
            $scope.click = "4";
        };
        $scope.file5Click = function() {
            $scope.click = "5";
        };


        $scope.downloadFile1 = function() {
            adminService.GetFile($scope.fileId1, $scope.recipientId).then(function (response) {
                var unsigned8Int = new Uint8Array(response);
                var downloadedFile = new Blob([unsigned8Int], { type: $scope.type1 });
                saveAs(downloadedFile, $scope.fileName1);
            }).catch(function(error) {
                console.log(error);
            });
        };
        $scope.downloadFile2 = function() {
            adminService.GetFile($scope.fileId2, $scope.recipientId).then(function (response) {
                var unsigned8Int = new Uint8Array(response);
                var downloadedFile = new Blob([unsigned8Int], { type: $scope.type2 });
                saveAs(downloadedFile, $scope.fileName2);
            }).catch(function(error) {
                console.log(error);
            });
        };
        $scope.downloadFile3 = function() {
            adminService.GetFile($scope.fileId3, $scope.recipientId).then(function (response) {
                var unsigned8Int = new Uint8Array(response);
                var downloadedFile = new Blob([unsigned8Int], { type: $scope.type3 });
                saveAs(downloadedFile, $scope.fileName3);
            }).catch(function(error) {
                console.log(error);
            });
        };
        $scope.downloadFile4 = function() {
            adminService.GetFile($scope.fileId4, $scope.recipientId).then(function (response) {
                var unsigned8Int = new Uint8Array(response);
                var downloadedFile = new Blob([unsigned8Int], { type: $scope.type4 });
                saveAs(downloadedFile, $scope.fileName4);
            }).catch(function(error) {
                console.log(error);
            });
        };
        $scope.downloadFile5 = function() {
            adminService.GetFile($scope.fileId5, $scope.recipientId).then(function (response) {
                var unsigned8Int = new Uint8Array(response);
                var downloadedFile = new Blob([unsigned8Int], { type: $scope.type5 });
                saveAs(downloadedFile, $scope.fileName5);
            }).catch(function(error) {
                console.log(error);
            });
        };
    });