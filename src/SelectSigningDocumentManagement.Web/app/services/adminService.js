﻿angular.module('SelectSigningDocumentManagement.Services').factory('adminService', function ($httpq, $http) {

    return {
        GetUsers: function (payload) {
            return $httpq.get("/users?" + "PageNumber=" + payload.PageNumber + "&PageSize=" + payload.PageSize + "&Field=" + payload.Field);
        },
        EnableUser: function (payload) {
            return $httpq.post("/users/enable", payload);
        },
        GetUser: function(id) {
            return $httpq.get("/user/" + id );
        },
        UpdateProfile: function(payload) {
            return $httpq.post("/user/", payload);
        },
        GetRol:function() {
            return $httpq.get("/rol");
        },
        GetFile: function (id, recipientId) {
            return $httpq.get("/file/" + id + '/' + recipientId);
        },
        SendMail: function(payload) {
            return $httpq.post("/sendMail", payload);
        },
        SendSms: function (payload) {
            return $httpq.post("/sendSms", payload);
        },
        GetSharedInfo: function(id) {
            return $httpq.get("/fileShared/" + id);
        },
        GetFileInfo: function (id) {
            return $httpq.get("/fileInfo/" + id);
        },
        SendSmsRetrievingPassword: function (payload) {
            return $httpq.post("/sendSmsRetrievingPassword", payload);
        },
        VerifyPassword: function (payload) {
            return $httpq.post("/verifyPassword", payload);
        },
        DeleteFiles: function() {
            return $httpq.post("/deleteFiles");
        }
    };
});