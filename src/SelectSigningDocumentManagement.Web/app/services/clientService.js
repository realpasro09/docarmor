﻿angular.module('SelectSigningDocumentManagement.Services').factory('clientService', function ($httpq) {

    return {
        NewClient: function (payload) {
            return $httpq.post("/createClient", payload);
        },
        GetContactInformationShortId: function () {
            return $httpq.get("/contactInformationShortId");
        },
        GetAllClients: function() {
            return $httpq.get("/getClientList");
        },
        DeleteClient: function(payload) {
            return $httpq.post("/deleteClient", payload);
        },
        NewClientPricings: function (payload) {
            return $httpq.post("/createClientPricings", payload)
        }
};
});