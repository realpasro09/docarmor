using AcklenAvenue.Commands;
using Nancy;
using SelectSigningDocumentManagement.Domain.Entities;
using SelectSigningDocumentManagement.Domain.Services;

using SelectSigningDocumentManagement.Web.Api.Infrastructure.Authentication;
using SelectSigningDocumentManagement.Web.Api.Infrastructure.Exceptions;
using SelectSigningDocumentManagement.Web.Api.Modules;

namespace SelectSigningDocumentManagement.Web.Api.Infrastructure
{
    public static class IdentityExtentions
    {
        public static IUserSession UserSession(this NancyModule module)
        {
            var user = module.Context.CurrentUser as LoggedInUserIdentity;
            if (user == null) throw new NoCurrentUserException();
            return user.UserSession;
        }

        public static UserLoginSession UserLoginSession(this NancyModule module)
        {
            var user = module.Context.CurrentUser as LoggedInUserIdentity;
            if (user == null || user.UserSession.GetType() != typeof(UserLoginSession)) throw new NoCurrentUserException();
            return (UserLoginSession) user.UserSession;
        }
    }
}