﻿using System;
using System.Linq;
using System.Web;
using AutoMapper.Internal;
using SelectSigningDocumentManagement.Web.Api.Requests.Google;

namespace SelectSigningDocumentManagement.Web.Api.Infrastructure.Authentication.Roles
{
    public class Feature
    {
         public string Description { get; set; }
    }
}