using System;

namespace SelectSigningDocumentManagement.Web.Api.Infrastructure.Authentication
{
    public class TokenDoesNotExistException : Exception
    {
    }
}