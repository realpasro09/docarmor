using System;

namespace SelectSigningDocumentManagement.Web.Api.Infrastructure.RestExceptions
{
    public class TokenDoesNotExistException : Exception
    {
    }
}