using System;

namespace SelectSigningDocumentManagement.Web.Api.Infrastructure.Exceptions
{
    public class NoCurrentUserException : Exception
    {
    }
}