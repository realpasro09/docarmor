using System;
using AutoMapper;
using Autofac;
using SelectSigningDocumentManagement.Domain.Entities;
using SelectSigningDocumentManagement.Web.Api.Requests;
using SelectSigningDocumentManagement.Web.Api.Requests.Admin;
using SelectSigningDocumentManagement.Web.Api.Responses.Admin;

namespace SelectSigningDocumentManagement.Web.Api.Infrastructure.Configuration
{
    public class ConfigureAutomapperMappings : IBootstrapperTask<ContainerBuilder>
    {
        #region IBootstrapperTask<ContainerBuilder> Members

        public Action<ContainerBuilder> Task
        {
            get
            {
                return container =>
                    {
                        Mapper.CreateMap<User, AdminUserResponse>();
                        Mapper.CreateMap<ContactInformationRequest, ContactInformation>();
                        Mapper.CreateMap<PricingRequest, Pricing>();
                        Mapper.CreateMap<UserAbility, UserAbilityRequest>().ReverseMap();
                    };
            }
        }

        #endregion
    }
}