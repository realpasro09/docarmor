﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using AcklenAvenue.Commands;
using AutoMapper;
using DotCMIS;
using DotCMIS.Client;
using DotCMIS.Client.Impl;
using DotCMIS.Data.Impl;
using Nancy;
using Nancy.Json;
using Nancy.ModelBinding;
using Nancy.Security;
using SelectSigningDocumentManagement.Data;
using SelectSigningDocumentManagement.Domain.Application.Commands;
using SelectSigningDocumentManagement.Domain.Entities;
using SelectSigningDocumentManagement.Domain.Services;
using SelectSigningDocumentManagement.Web.Api.Infrastructure;
using SelectSigningDocumentManagement.Web.Api.Requests.Admin;
using SelectSigningDocumentManagement.Web.Api.Responses.Admin;
using Twilio;
using ShareFile = SelectSigningDocumentManagement.Domain.Entities.ShareFile;

namespace SelectSigningDocumentManagement.Web.Api.Modules
{
    public class AdminModule : NancyModule
    {
        static byte[] ReadFully(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public AdminModule(IReadOnlyRepository readOnlyRepository, IMappingEngine mappingEngine,
            ICommandDispatcher commandDispatcher, IShareFileCreator shareFileCreator, 
            IShareFile shareFile, IRecipientNotifier recipientNotifier, IWriteableRepository writeableRepository)
        {
            Post["/deleteFiles"] = x =>
            {
                var parameters = new Dictionary<string, string>();
                parameters[SessionParameter.BindingType] = BindingType.AtomPub;
                parameters[SessionParameter.AtomPubUrl] = "http://74.212.223.106:8080/alfresco/cmisatom";
                parameters[SessionParameter.User] = "admin";
                parameters[SessionParameter.Password] = "P@$$word";
                var factory = SessionFactory.NewInstance();
                var session = factory.GetRepositories(parameters)[0].CreateSession();

                var rootFolder = session.GetRootFolder();
                foreach (var folder in rootFolder.GetChildren())
                {
                    if (!folder.Name.Equals("SelectSigningDocumentManagement")) continue;
                    var myFolder = folder as IFolder;
                    if (myFolder == null) continue;
                    var childrenFolders = myFolder.GetChildren();

                    foreach (var children in childrenFolders)
                    {
                        var creationDate = children.CreationDate;
                        var today = DateTime.Now;
                        var maxOldDate = today.AddDays(-11);
                        if (children.Name == "20151013_20565")
                        {
                            
                        }
                        if (creationDate < maxOldDate)
                        {
                            switch (children.ObjectType.Id)
                            {
                                case "cmis:folder":
                                    var oldFolder = children as IFolder;
                                    if (oldFolder != null)
                                    {
                                        var childs = oldFolder.GetChildren();
                                        foreach (var oldFileInFolder in childs.Select(child => child as IDocument))
                                        {
                                            if (oldFileInFolder != null) oldFileInFolder.Delete(true);
                                            break;
                                        }
                                    }
                                    if (oldFolder != null) oldFolder.Delete(true);
                                    break;
                                case "cmis:document":
                                    var oldFile = children as IDocument;
                                    if (oldFile != null) oldFile.Delete(true);
                                    break;
                            }
                        }
                    }
                }

                return "OK";
            };

            Post["/fileupload"] = x =>
            {
                var file = Request.Files.FirstOrDefault();
                var commonShareId = Request.Form["commonShareId"];
                string message = Request.Form["message"];
                var password = Request.Form["password"];

                if (message.Contains("undefined"))
                {
                    message = message.Split('_')[0]+'_';
                }

                var parameters = new Dictionary<string, string>();
                parameters[SessionParameter.BindingType] = BindingType.AtomPub;
                parameters[SessionParameter.AtomPubUrl] = "http://74.212.223.106:8080/alfresco/cmisatom";
                parameters[SessionParameter.User] = "admin";
                parameters[SessionParameter.Password] = "P@$$word";
                var factory = SessionFactory.NewInstance();
                var session = factory.GetRepositories(parameters)[0].CreateSession();

                var rootFolder = session.GetRootFolder();
                var shareId = "";
                foreach (var folder in rootFolder.GetChildren())
                {
                    if (!folder.Name.Equals("SelectSigningDocumentManagement")) continue;
                    var myFolder = folder as IFolder;

                    IDictionary<string, object> properties = new Dictionary<string, object>();
                    if (file == null) continue;
                    properties[PropertyIds.Name] = file.Name;
                    properties[PropertyIds.ObjectTypeId] = "cmis:document";


                    var contentStream = new ContentStream
                    {
                        FileName = file.Name,
                        MimeType = file.ContentType,
                        Length = file.Value.Length,
                        Stream = file.Value
                    };

                    if (myFolder == null) continue;

                    try
                    {
                        //create newFolder
                        var date = DateTime.Now;

                        var year = date.Year.ToString();
                        var month = date.Month.ToString();
                        var day = date.Day.ToString();
                        var hours = date.Hour.ToString();
                        var minutes = date.Minute.ToString();
                        var seconds = date.Second.ToString();

                        IDictionary<string, object> folderProperties = new Dictionary<string, object>();
                        folderProperties[PropertyIds.Name] = year + month + day + "_" + hours + minutes + seconds;
                        folderProperties[PropertyIds.ObjectTypeId] = "cmis:folder";

                        var newFolder = myFolder.CreateFolder(folderProperties);
                        var doc = newFolder.CreateDocument(properties, contentStream, null);
                        var id = doc.VersionSeriesId.Split('/')[3];

                        shareId = string.IsNullOrEmpty(commonShareId) ?
                            shareFileCreator.ShareFile(id, doc.ContentStreamFileName, doc.ContentStreamMimeType, message, password) :
                            shareFileCreator.ShareFile(id, doc.ContentStreamFileName, doc.ContentStreamMimeType, message, password, commonShareId);
                    }
                    catch (Exception e) 
                    {
                        throw new Exception(e.Message);
                    }
                    
                }


                return shareId;

            };

            Post["/sendMail"] =
                _ =>
                {

                    var frontEndRequest = this.Bind<EmailsRequest>();


                    if (frontEndRequest.Message.Contains("undefined"))
                    {
                        frontEndRequest.Message = frontEndRequest.Message.Split('_')[0] + '_';
                    }
                    var shortId = frontEndRequest.ShareLink.Split('/');
                    foreach (var email in frontEndRequest.Emails)
                    {
                        if (string.IsNullOrEmpty(email)) continue;
                        
                        var recipientId = recipientNotifier.CreateRecipientNotifier(email, shortId[5], frontEndRequest.EmailSender);
                        SendEmailToRecipients(frontEndRequest, email, recipientId);
                    }
                    
                    SendEmailToSender(frontEndRequest);

                    var shareFil = readOnlyRepository.Query<ShareFile>(x => x.ShareId == shortId[5]).ToList();
                    foreach (var file in shareFil)
                    {
                        file.SentToSender = true;
                        writeableRepository.Update(file);
                    }

                    return null;
                };
            Post["/sendSms"] =
                _ =>
                {

                    var request = this.Bind<SmsRequest>();

                    SendSmsEmail(request.PhoneNumberEmail, request.Password, request.FileName);
                    var shareFil = readOnlyRepository.Query<ShareFile>(x => x.ShareId == request.ShareId).ToList();
                    foreach (var file in shareFil)
                    {
                        file.Phone = request.PhoneNumberEmail;
                        file.ShareName = request.FileName;
                        writeableRepository.Update(file);
                    }
                    
                    return null;
                };
            Post["/sendSmsRetrievingPassword"] =
               _ =>
               {

                   var request = this.Bind<SmsRequest>();

                   SendSmsPasswordRetrieve(request.PhoneNumberEmail, request.Password, request.FileName);

                   return null;
               };
            Post["/verifyPassword"] =
                _ =>
                {
                    var request = this.Bind<VerifyRequest>();
                    var shareItem = readOnlyRepository.Query<ShareFile>(x => x.ShareId == request.ShareId).First();
                    var recipient = readOnlyRepository.Query<RecipientNotification>(x => x.RecipientId == request.RecipientId).First();

                    if (request.PhoneToVerify == shareItem.Phone && request.EmailToVerify == recipient.Email)
                    {
                        return shareItem;
                    }
                    return null;
                };
            Get["/fileShared/{sharedId}"] =
                _ =>
                {
                    var sharedFiles = shareFile.GetFromShareId(_.sharedId);
                    var mappedSharedFiles = new List<SharedFileResponse>();
                    foreach (ShareFile file in sharedFiles)
                    {
                        mappedSharedFiles.Add(new SharedFileResponse()
                        {
                            FileId = file.FileId,
                            FileName = file.FileName,
                            FileType = file.FileType,
                            Message = file.Message,
                            Password = file.Password
                        });
                    }
                    return mappedSharedFiles;
                };

            Get["/fileInfo/{fileid}"] =
                _ =>
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters[SessionParameter.BindingType] = BindingType.AtomPub;
                    parameters[SessionParameter.AtomPubUrl] = "http://74.212.223.106:8080/alfresco/cmisatom";
                    parameters[SessionParameter.User] = "admin";
                    parameters[SessionParameter.Password] = "P@$$word";
                    var factory = SessionFactory.NewInstance();
                    var session = factory.GetRepositories(parameters)[0].CreateSession();

                    var existingFile = (Document) session.GetObject(_.fileid);

                    var mycontentStream = existingFile.GetContentStream();
                    var fileResponse = new FileResponse()
                    {
                        Bytes = null,
                        Content = null,
                        FileType = mycontentStream.MimeType,
                        FileName = mycontentStream.FileName
                    };
                    return fileResponse;
                };
            Get["/file/{fileid}/{recipientId}"] =
                _ =>
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters[SessionParameter.BindingType] = BindingType.AtomPub;
                    parameters[SessionParameter.AtomPubUrl] = "http://74.212.223.106:8080/alfresco/cmisatom";
                    parameters[SessionParameter.User] = "admin";
                    parameters[SessionParameter.Password] = "P@$$word";
                    var factory = SessionFactory.NewInstance();
                    var session = factory.GetRepositories(parameters)[0].CreateSession();

                    var existingFile = (Document)session.GetObject(_.fileid);

                    var mycontentStream = existingFile.GetContentStream();

                    var fileBytes = ReadFully(mycontentStream.Stream);
                    
                    JsonSettings.MaxJsonLength = Int32.MaxValue;

                    if (_.recipientId == "undefined") return fileBytes;

                    if (recipientNotifier.CheckNotification(_.recipientId)) return fileBytes;

                    string recipientId = _.recipientId;
                    string shareId =
                        readOnlyRepository.Query<RecipientNotification>(x => x.RecipientId == recipientId).First().SharedId;
                    var message = readOnlyRepository.Query<ShareFile>(x => x.ShareId == shareId).First().Message;

                    NotifySender(recipientNotifier.GetRecipientEmailByRecipientId(_.recipientId), recipientNotifier.GetSenderByRecipientId(_.recipientId), message);

                    recipientNotifier.UpdateNotify(_.recipientId);

                    return fileBytes;

                };
            Get["/users"] =
                _ =>
                    {
                        this.RequiresClaims(new[] { "Administrator" });
                        var request = this.Bind<AdminUsersRequest>();
                      
                        var parameter = Expression.Parameter(typeof(User), "User");
                        var mySortExpression = Expression.Lambda<Func<User, object>>(Expression.Property(parameter, request.Field), parameter);
                        
                        IQueryable<User> users =
                            readOnlyRepository.Query<User>(x => x.Name != this.UserLoginSession().User.Name).AsQueryable();

                        var orderedUsers = users.OrderBy(mySortExpression);

                        IQueryable<User> pagedUsers = orderedUsers.Skip(request.PageSize * (request.PageNumber - 1)).Take(request.PageSize);

                        List<AdminUserResponse> mappedItems = mappingEngine
                            .Map<IQueryable<User>, IEnumerable<AdminUserResponse>>(pagedUsers).ToList();

                        return new AdminUsersListResponse(mappedItems);
                    };

            Post["/users/enable"] =
                _ =>
                {
                   this.RequiresClaims(new[] {"Administrator"});
                    var request = this.Bind<AdminEnableUsersRequest>();
                    if (request.Enable)
                    {
                        commandDispatcher.Dispatch(this.UserSession(), new EnableUser(request.Id)); 
                    }
                    else
                    {
                        commandDispatcher.Dispatch(this.UserSession(), new DisableUser(request.Id));
                    }
                
                    return null;
                };

            Get["/user/{userId}"] =
                _ =>
                {
                    var userId = Guid.Parse((string)_.userId);
                    var user = readOnlyRepository.GetById<User>(userId);
                    var mappedUser = mappingEngine
                            .Map<User, AdminUserResponse>(user);
                    return mappedUser;
                };

            Post["/user"] =
                _ =>
                {
                    var request = this.Bind<AdminUpdateUserRequest>();
                    commandDispatcher.Dispatch(this.UserSession(), new UpdateUserProfile(request.Id, request.Name, request.Email));
                    return null;
                };

            

        }
        private static void SendSmsEmail(string phoneNumberEmail, string password, string filename)
        {
            // Find your Account Sid and Auth Token at twilio.com/user/account 
            const string accountSid = "AC061eb402e4220d6c3d9c5dc425d6473f";
            const string authToken = "3f9a02a47a552838e21db03d6e4e9aca";
            var twilio = new TwilioRestClient(accountSid, authToken);

            twilio.SendMessage("+13213514000", "+1" + phoneNumberEmail,
                "Some files were shared with you from DocArmor. In your email inbox will be a link to the files. Check spam or junk mail if you do not see the link.  File name is: " + filename + ".  The password to download the files is: " + password);
        }

        private static void SendSmsPasswordRetrieve(string phoneNumberEmail, string password, string filename)
        {
            // Find your Account Sid and Auth Token at twilio.com/user/account 
            const string accountSid = "AC061eb402e4220d6c3d9c5dc425d6473f";
            const string authToken = "3f9a02a47a552838e21db03d6e4e9aca";
            var twilio = new TwilioRestClient(accountSid, authToken);
            twilio.SendMessage("+13213514000", "+1"+ phoneNumberEmail,
                "Password retrieval from DocArmor.  File name is: " + filename + ".  The password to download the files is: " + password);
        }
        private static void SendEmailToSender(EmailsRequest frontEndRequest)
        {
            var emailTemplate = new EmailTemplate();
            var fromAddress = new MailAddress("document@docarmor.com", "Doc Armor");
            var toAddress = new MailAddress(frontEndRequest.EmailSender);

            const string fromPassword = "V0oRK9wbTcrT7P";
            var emails = frontEndRequest.Emails.Aggregate("", (current, email) => current + (email + ","));
            emails = emails.Substring(0, emails.Length - 1);
            const string subject = "Thanks for using Doc Armor";
            var body = emailTemplate.GenerateSenderEmail(emails, frontEndRequest.ShareLink, frontEndRequest.Message);

            var plainView = AlternateView.CreateAlternateViewFromString
(System.Text.RegularExpressions.Regex.Replace(body, @"<(.|\n)*?>", string.Empty), null, "text/plain");
            var htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

            var smtp = new SmtpClient
            {
                Host = "mail.docarmor.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.AlternateViews.Add(plainView);
                message.AlternateViews.Add(htmlView);
                message.IsBodyHtml = true;
                smtp.EnableSsl = false;
                smtp.Send(message);
            }
        }

        private static void SendEmailToRecipients(EmailsRequest frontEndRequest, string email, string recipientId)
        {
            var emailTemplate = new EmailTemplate();
            var fromAddress = new MailAddress("document@docarmor.com", "Doc Armor");
            var toAddress = new MailAddress(email);
            const string fromPassword = "V0oRK9wbTcrT7P";
            const string subject = "You just receive a file via‏ Doc Armor";
            var body = emailTemplate.GenerateEmail(frontEndRequest.EmailSender, frontEndRequest.ShareLink + "-" + recipientId, frontEndRequest.Message);
            var plainView = AlternateView.CreateAlternateViewFromString
(System.Text.RegularExpressions.Regex.Replace(body, @"<(.|\n)*?>", string.Empty), null, "text/plain");
            var htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

            var smtp = new SmtpClient
            {
                Host = "mail.docarmor.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.AlternateViews.Add(plainView);
                message.AlternateViews.Add(htmlView);
                message.IsBodyHtml = true;
                smtp.EnableSsl = false;
                smtp.Send(message);
            }
        }

        private static void NotifySender(string recipient, string sender,string message1)
        {
            var recipientName = recipient.Split('@');
            var emailTemplate = new EmailTemplate();
            var fromAddress = new MailAddress("document@docarmor.com", "Doc Armor");
            var toAddress = new MailAddress(sender);
            const string fromPassword = "V0oRK9wbTcrT7P";
            var subject = "Download confirmation from " + recipientName[0];
            var body = emailTemplate.GenerateConfirmation(recipient, message1);
            var plainView = AlternateView.CreateAlternateViewFromString
(System.Text.RegularExpressions.Regex.Replace(body, @"<(.|\n)*?>", string.Empty), null, "text/plain");
            var htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

            var smtp = new SmtpClient
            {
                Host = "mail.docarmor.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.AlternateViews.Add(plainView);
                message.AlternateViews.Add(htmlView);
                message.IsBodyHtml = true;
                smtp.EnableSsl = false;
                smtp.Send(message);
            }
        }
    }
}