using System;
using System.Linq;
using AutoMapper;
using Nancy;
using Nancy.ModelBinding;
using SelectSigningDocumentManagement.Domain.Entities;
using SelectSigningDocumentManagement.Domain.Services;
using SelectSigningDocumentManagement.Web.Api.Requests.Admin;
using System.Collections.Generic;

namespace SelectSigningDocumentManagement.Web.Api.Modules
{
    public class ClientModule : NancyModule
    {
        private static string GenerateId()
        {
            var i = Guid.NewGuid().ToByteArray().Aggregate<byte, long>(1, (current, b) => current * (b + 1));
            return string.Format("{0:x}", i - DateTime.Now.Ticks);
        }

        public ClientModule(IReadOnlyRepository readOnlyRepository, IMappingEngine mappingEngine, IWriteableRepository writeableRepository, IIdentityGenerator<Guid> guIdentityGenerator)
        {
            Get["/contactInformationShortId"] =
                _ => GenerateId();

            Post["/createClient"] =
                _ =>
                {
                    var request = this.Bind<ClientInformationRequest>();
                    var contactInformation = mappingEngine.Map<ContactInformationRequest, ContactInformation>(request.ContactInformation);
                    contactInformation.Id = guIdentityGenerator.Generate();
                    writeableRepository.Create(contactInformation);
                    return "OK";
                };
            Post["/createClientPricings"] =
                _ =>
                {
                    var request = this.Bind<ClientInformationRequest>();
                    var pricingInformation = mappingEngine.Map<PricingRequest, Pricing>(request.PricingInformation);
                    pricingInformation.Id = guIdentityGenerator.Generate();
                    writeableRepository.Create(pricingInformation);
                    return "OK";
                };
            Get["/getClientList"] =
                _ =>
                {
                    var contactInformation = readOnlyRepository.GetAll<ContactInformation>().ToList();
                    return contactInformation;
                };
            Post["/deleteClient"] =
                _ =>
                {
                    var request = this.Bind<DeleteClientRequest>();
                    writeableRepository.Delete<ContactInformation>(request.ClientId);
                    return "OK";
                };
        }
    }
}