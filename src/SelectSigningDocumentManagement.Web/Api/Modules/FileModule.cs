﻿using System.Collections.Generic;
using System.Linq;
using DotCMIS;
using DotCMIS.Client;
using DotCMIS.Client.Impl;
using DotCMIS.Data.Impl;
using Nancy;
using SelectSigningDocumentManagement.Domain.Entities;
using SelectSigningDocumentManagement.Domain.Services;

namespace SelectSigningDocumentManagement.Web.Api.Modules
{
    public class FileModule: NancyModule
    {
        public FileModule(IReadOnlyRepository readOnlyRepository, IWriteableRepository writeableRepository)
        {
            Post["/serviceAgreementUpload"] = x =>
            {
                var file = Request.Files.FirstOrDefault();
                string clientId = Request.Form["clientId"].ToString();

                var parameters = new Dictionary<string, string>();
                parameters[SessionParameter.BindingType] = BindingType.AtomPub;
                parameters[SessionParameter.AtomPubUrl] = "http://74.212.223.106:8080/alfresco/cmisatom";
                parameters[SessionParameter.User] = "admin";
                parameters[SessionParameter.Password] = "P@$$word";
                var factory = SessionFactory.NewInstance();
                var session = factory.GetRepositories(parameters)[0].CreateSession();

                var rootFolder = session.GetRootFolder();
                foreach (var folder in rootFolder.GetChildren())
                {
                    if (folder.Name.Equals("SSDM"))
                    {
                        foreach (var childrenFolder in ((IFolder)folder).GetChildren())
                        {
                            if (!childrenFolder.Name.Equals("ServiceAgreements")) continue;
                            var serviceAgreementsFolder = childrenFolder as IFolder;
                            IDictionary<string, object> properties = new Dictionary<string, object>();
                            if (file == null) continue;
                            properties[PropertyIds.Name] = file.Name;
                            properties[PropertyIds.ObjectTypeId] = "cmis:document";
                                
                            var contentStream = new ContentStream
                            {
                                FileName = file.Name,
                                MimeType = file.ContentType,
                                Length = file.Value.Length,
                                Stream = file.Value
                            };

                            try
                            {
                                if (serviceAgreementsFolder != null)
                                {
                                    var existingFile = session.GetObjectByPath(serviceAgreementsFolder.Path + "/" + file.Name);
                                    existingFile.Delete(true);
                                }
                            }
                            catch
                            {
                                // ignored
                            }

                            if (serviceAgreementsFolder == null) continue;
                            var doc = serviceAgreementsFolder.CreateDocument(properties, contentStream, null);
                            var clientInfo = readOnlyRepository.Query<ContactInformation>(xx => xx.ClientId == clientId);
                            var id = doc.VersionSeriesId.Split('/')[3];
                            clientInfo.First().ServiceAgreementName = file.Name;
                            clientInfo.First().ServiceAgreementNameFileId = id;
                            writeableRepository.Update(clientInfo.First());
                        }
                    }
                }

                return "OK";

            };
        }
    }
}