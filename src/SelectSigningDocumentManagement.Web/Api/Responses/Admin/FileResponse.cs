﻿using System.Net.Http;

namespace SelectSigningDocumentManagement.Web.Api.Responses.Admin
{
    public class FileResponse
    {
        public byte[] Bytes { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public StreamContent Content { get; set; }
    }
}