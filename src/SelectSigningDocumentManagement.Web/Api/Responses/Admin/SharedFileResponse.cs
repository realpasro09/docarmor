﻿namespace SelectSigningDocumentManagement.Web.Api.Responses.Admin
{
    public class SharedFileResponse
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Message { get; set; }
        public string Password { get; set; }
    }
}