﻿namespace SelectSigningDocumentManagement.Web.Api
{
    public interface IBaseUrlProvider
    {
        string GetBaseUrl();
    }
}