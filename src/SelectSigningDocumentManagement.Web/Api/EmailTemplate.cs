﻿using System;
using System.Text;

namespace SelectSigningDocumentManagement.Web.Api
{
    public class EmailTemplate
    {
        readonly string _template = @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
      <style type='text/css'>
      body {
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       margin:0 !important;
       width: 100% !important;
       -webkit-text-size-adjust: 100% !important;
       -ms-text-size-adjust: 100% !important;
       -webkit-font-smoothing: antialiased !important;
     }
     .tableContent img {
       border: 0 !important;
       display: block !important;
       outline: none !important;
     }
     a{
      color:#382F2E;
    }

    p, h1{
      color:#382F2E;
      margin:0;
    }
 p{
      text-align:left;
      color:#999999;
      font-size:14px;
      font-weight:normal;
      line-height:19px;
    }

    a.link1{
      color:#382F2E;
    }
    a.link2{
      font-size:16px;
      text-decoration:none;
      color:#ffffff;
    }

    h2{
      text-align:left;
       color:#222222; 
       font-size:19px;
      font-weight:normal;
    }
    div,p,ul,h1{
      margin:0;
    }

    .bgBody{
      background: #ffffff;
    }
    .bgItem{
      background: #ffffff;
    }

    </style>
<script type='colorScheme' class='swatch active'>
{
    'name':'Default',
    'bgBody':'ffffff',
    'link':'382F2E',
    'color':'999999',
    'bgItem':'ffffff',
    'title':'222222'
}
</script>
  </head>
  <body paddingwidth='0' paddingheight='0'   style='padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent bgBody' align='center'  style='font-family:Helvetica, Arial,serif;'>

      
      <tr><td height='35'></td></tr>

      <tr>
        <td>
          <table width='600' border='0' cellspacing='0' cellpadding='0' align='center' class='bgItem'>
            <tr>
              <td width='40'></td>
              <td width='520'>
                <table width='520' border='0' cellspacing='0' cellpadding='0' align='center'>
                  <tr><td height='75'></td></tr>
                  <tr>
                    <td class='movableContentContainer' valign='top'>
                      <div class='movableContent'>
                        <table width='520' border='0' cellspacing='0' cellpadding='0' align='center'>
                          <tr><td height='55'></td></tr>
                          <tr>
                            <td align='left'>
                              <div class='contentEditableContainer contentTextEditable'>
                                <div class='contentEditable' align='center'>
                                  <h2 >[SENDER] sent you some files via DocArmor. The files will be deleted on [EXPIREDDATE]. Click on the Show Files button below to access the files.</h2>
                                </div>
                              </div>
                            </td>
                          </tr>

                          <tr><td height='15'> </td></tr>

                          <tr>
                            <td align='left'>
                              <div class='contentEditableContainer contentTextEditable'>
                                <div class='contentEditable' align='center'>
                                  <p  style='text-align:left;color:black;font-size:16px;font-weight:bolds;line-height:19px;'>
                                    File Name: [FILENAME] 
                                  </p>
                                  <p  style='text-align:left;color:black;font-size:16px;font-weight:bolds;line-height:19px;'>
                                    Message: 
                                  </p>
                                  <p  style='text-align:left;color:#999999;font-size:14px;font-weight:normal;line-height:19px;'>
                                    [MESSAGE] 
                                  </p>
                                  <br/>
                                  <br/>
                                  <p  style='text-align:left;color:#999999;font-size:14px;font-weight:normal;line-height:19px;'>
                                    Please click the following link to see the files shared with you.
                                  </p>
                                </div>
                              </div>
                            </td>
                          </tr>

                          <tr><td height='55'></td></tr>

                          <tr>
                            <td align='center'>
                              <table>
                                <tr>
                                  <td align='center' bgcolor='#289CDC' style='background:#289CDC; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'>
                                    <div class='contentEditableContainer contentTextEditable'>
                                      <div class='contentEditable' align='center'>
                                        <a target='_blank' href='[SHAREDLINK]' class='link2' style='color:#ffffff;'>Show Files</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr><td height='20'></td></tr>
                        </table>
                      </div>
                    </td>
                  </tr>


                </table>
              </td>
              <td width='40'></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr><td height='88'></td></tr>


    </table>
      </body>
      </html>";

        readonly string _template2 = @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
      <style type='text/css'>
      body {
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       margin:0 !important;
       width: 100% !important;
       -webkit-text-size-adjust: 100% !important;
       -ms-text-size-adjust: 100% !important;
       -webkit-font-smoothing: antialiased !important;
     }
     .tableContent img {
       border: 0 !important;
       display: block !important;
       outline: none !important;
     }
     a{
      color:#382F2E;
    }

    p, h1{
      color:#382F2E;
      margin:0;
    }
 p{
      text-align:left;
      color:#999999;
      font-size:14px;
      font-weight:normal;
      line-height:19px;
    }

    a.link1{
      color:#382F2E;
    }
    a.link2{
      font-size:16px;
      text-decoration:none;
      color:#ffffff;
    }

    h2{
      text-align:left;
       color:#222222; 
       font-size:19px;
      font-weight:normal;
    }
    div,p,ul,h1{
      margin:0;
    }

    .bgBody{
      background: #ffffff;
    }
    .bgItem{
      background: #ffffff;
    }

    </style>
<script type='colorScheme' class='swatch active'>
{
    'name':'Default',
    'bgBody':'ffffff',
    'link':'382F2E',
    'color':'999999',
    'bgItem':'ffffff',
    'title':'222222'
}
</script>
  </head>
  <body paddingwidth='0' paddingheight='0'   style='padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent bgBody' align='center'  style='font-family:Helvetica, Arial,serif;'>

      
      <tr><td height='35'></td></tr>

      <tr>
        <td>
          <table width='600' border='0' cellspacing='0' cellpadding='0' align='center' class='bgItem'>
            <tr>
              <td width='40'></td>
              <td width='520'>
                <table width='520' border='0' cellspacing='0' cellpadding='0' align='center'>

                  <tr><td height='75'></td></tr>

                  <tr>
                    <td class='movableContentContainer' valign='top'>                    

                      <div class='movableContent'>
                        <table width='520' border='0' cellspacing='0' cellpadding='0' align='center'>
                          <tr><td height='55'></td></tr>
                          <tr>
                            <td align='left'>
                              <div class='contentEditableContainer contentTextEditable'>
                                <div class='contentEditable' align='center'>
                                  <h2 >Files successfully sent to [EMAILS], files will be deleted on [EXPIREDDATE]</h2>
                                  <p  style='text-align:left;color:black;font-size:16px;font-weight:bolds;line-height:19px;'>
                                    File Name: [FILENAME] 
                                  </p>
                                </div>
                              </div>
                            </td>
                          </tr>

                          <tr><td height='15'> </td></tr>

                          <tr>
                            <td align='left'>
                              <div class='contentEditableContainer contentTextEditable'>
                                <div class='contentEditable' align='center'>
                                  <p  style='text-align:left;color:#999999;font-size:14px;font-weight:normal;line-height:19px;'>
                                    As soon as the recipient has downloaded your file, you will receive a confirmation email.
                                  </p>
                                </div>
                              </div>
                            </td>
                          </tr>

                          <tr><td height='55'></td></tr>

                          <tr>
                            <td align='center'>
                              <table>
                                <tr>
                                  <td align='center' bgcolor='#289CDC' style='background:#289CDC; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'>
                                    <div class='contentEditableContainer contentTextEditable'>
                                      <div class='contentEditable' align='center'>
                                        <a target='_blank' href='[SHAREDLINK]' class='link2' style='color:#ffffff;'>Show Shared Files</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr><td height='20'></td></tr>
                        </table>
                      </div>
                    </td>
                  </tr>

                </table>
              </td>
              <td width='40'></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr><td height='88'></td></tr>


    </table>
      </body>
      </html>";

        readonly string _template3 = @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
      <style type='text/css'>
      body {
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       margin:0 !important;
       width: 100% !important;
       -webkit-text-size-adjust: 100% !important;
       -ms-text-size-adjust: 100% !important;
       -webkit-font-smoothing: antialiased !important;
     }
     .tableContent img {
       border: 0 !important;
       display: block !important;
       outline: none !important;
     }
     a{
      color:#382F2E;
    }

    p, h1{
      color:#382F2E;
      margin:0;
    }
 p{
      text-align:left;
      color:#999999;
      font-size:14px;
      font-weight:normal;
      line-height:19px;
    }

    a.link1{
      color:#382F2E;
    }
    a.link2{
      font-size:16px;
      text-decoration:none;
      color:#ffffff;
    }

    h2{
      text-align:left;
       color:#222222; 
       font-size:19px;
      font-weight:normal;
    }
    div,p,ul,h1{
      margin:0;
    }

    .bgBody{
      background: #ffffff;
    }
    .bgItem{
      background: #ffffff;
    }

    </style>
<script type='colorScheme' class='swatch active'>
{
    'name':'Default',
    'bgBody':'ffffff',
    'link':'382F2E',
    'color':'999999',
    'bgItem':'ffffff',
    'title':'222222'
}
</script>
  </head>
  <body paddingwidth='0' paddingheight='0'   style='padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent bgBody' align='center'  style='font-family:Helvetica, Arial,serif;'>

      
      <tr><td height='35'></td></tr>

      <tr>
        <td>
          <table width='600' border='0' cellspacing='0' cellpadding='0' align='center' class='bgItem'>
            <tr>
              <td width='40'></td>
              <td width='520'>
                <table width='520' border='0' cellspacing='0' cellpadding='0' align='center'>

                  <tr><td height='75'></td></tr>

                  <tr>
                    <td class='movableContentContainer' valign='top'>                  
                      <div class='movableContent'>
                        <table width='520' border='0' cellspacing='0' cellpadding='0' align='center'>
                          <tr><td height='55'></td></tr>
                          <tr>
                            <td align='left'>
                              <div class='contentEditableContainer contentTextEditable'>
                                <div class='contentEditable' align='center'>
                                  <h2 style='text-align: center;'>[RECIPIENT] downloaded your files</h2>
                                  <p style='text-align:center;color:black;font-size:16px;font-weight:bolds;line-height:19px;'>
                                    File Name: [FILENAME]
                                  </p>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>

                </table>
              </td>
              <td width='40'></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr><td height='88'></td></tr>


    </table>
      </body>
      </html>";

        public string GenerateConfirmation(string recipient, string message)
        {
            var splitMessage = message.Split('_');
            var builder = new StringBuilder(_template3);
            builder.Replace("[RECIPIENT]", recipient);
            builder.Replace("[FILENAME]", splitMessage[0]);
            return builder.ToString();
        }

        public string GenerateEmail(string sender, string sharedLink, string message)
        {
            var splitMessage = message.Split('_');
            var builder = new StringBuilder(_template);
            builder.Replace("[SENDER]", sender);
            builder.Replace("[SHAREDLINK]", sharedLink);
            builder.Replace("[FILENAME]", splitMessage[0]);
            builder.Replace("[MESSAGE]", splitMessage.Length == 1?"":splitMessage[1]);
            builder.Replace("[EXPIREDDATE]", DateTime.Now.AddDays(10).ToShortDateString());
            return builder.ToString();
        }

        public string GenerateSenderEmail(string emails, string shareLink, string message)
        {
            var splitMessage = message.Split('_');
            var builder = new StringBuilder(_template2);
            builder.Replace("[EMAILS]", emails);
            builder.Replace("[SHAREDLINK]", shareLink);
            builder.Replace("[FILENAME]", splitMessage[0]);
            builder.Replace("[EXPIREDDATE]", DateTime.Now.AddDays(10).ToShortDateString());
            return builder.ToString();
        }
    }
}