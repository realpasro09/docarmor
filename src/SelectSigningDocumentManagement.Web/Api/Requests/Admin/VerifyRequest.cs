namespace SelectSigningDocumentManagement.Web.Api.Requests.Admin
{
    public class VerifyRequest
    {
        public string ShareId { get; set; }
        public string RecipientId { get; set; }
        public string PhoneToVerify { get; set; }
        public string EmailToVerify { get; set; }
    }
}