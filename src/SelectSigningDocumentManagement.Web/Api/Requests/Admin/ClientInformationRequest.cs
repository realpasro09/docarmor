﻿namespace SelectSigningDocumentManagement.Web.Api.Requests.Admin
{
    public class ClientInformationRequest
    {
        public ContactInformationRequest ContactInformation { get; set; }
        public PricingRequest PricingInformation { get; set; }
    }
}