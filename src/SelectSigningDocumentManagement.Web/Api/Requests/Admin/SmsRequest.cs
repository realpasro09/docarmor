namespace SelectSigningDocumentManagement.Web.Api.Requests.Admin
{
    public class SmsRequest
    {
        public string PhoneNumberEmail { get; set; }
        public string Password { get; set; }
        public string FileName { get; set; }
        public string ShareId { get; set; }
    }
}