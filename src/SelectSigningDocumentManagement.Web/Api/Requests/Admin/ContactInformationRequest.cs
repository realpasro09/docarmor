﻿namespace SelectSigningDocumentManagement.Web.Api.Requests.Admin
{
    public class ContactInformationRequest
    {
        #region Contact Information

        public string ClientId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Title { get; set; }

        public string Office { get; set; }

        public string Ext { get; set; }

        public string DirectLine { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string OnlinePassword { get; set; }

        public string ServiceAgreementName { get; set; }

        public string ServiceAgreementNameFileId { get; set; }

        #endregion

        #region Branch Address

        public string CompanyBranch { get; set; }

        public string StreetBranch { get; set; }

        public string CityBranch { get; set; }

        public string StateBranch { get; set; }

        public string ZipCodeBranch { get; set; }

        #endregion

        #region ShippingAddress

        public string CompanyShipping { get; set; }

        public string StreetShipping { get; set; }

        public string CityShipping { get; set; }

        public string StateShipping { get; set; }

        public string ZipCodeShipping { get; set; }

        public string Attention { get; set; }

        #endregion

        #region Shipping Info

        public string Courier { get; set; }

        public string AccountNumber { get; set; }

        #endregion

        #region Special Instructions

        public string InternalNotes { get; set; }

        public string SigningInstructions { get; set; }
        
        #endregion

    }
}