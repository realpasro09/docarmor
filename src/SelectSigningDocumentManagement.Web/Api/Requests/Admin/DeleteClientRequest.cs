﻿using System;

namespace SelectSigningDocumentManagement.Web.Api.Requests.Admin
{
    public class DeleteClientRequest
    {
        public Guid ClientId { get; set; }
    }
}