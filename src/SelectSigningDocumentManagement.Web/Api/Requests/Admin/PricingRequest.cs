﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SelectSigningDocumentManagement.Web.Api.Requests.Admin
{
    public class PricingRequest
    {
        public string FeeCode { get; set; }

        public string FeeDescription { get; set; }

        public string Amount { get; set; }

        public string ClientID { get; set; }

        public Guid Id { get; set; }
    }
}