using System.Collections.Generic;

namespace SelectSigningDocumentManagement.Web.Api.Requests.Admin
{
    public class EmailsRequest
    {
        public string ShareLink { get; set; }
        public string EmailSender { get; set; }
        public List<string> Emails { get; set; }
        public string Message { get; set; }
    }
}