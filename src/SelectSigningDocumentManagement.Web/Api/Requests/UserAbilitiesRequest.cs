using System;
using System.Collections.Generic;
using SelectSigningDocumentManagement.Web.Api.Modules;

namespace SelectSigningDocumentManagement.Web.Api.Requests
{
    public class UserAbilitiesRequest
    {
        public IEnumerable<UserAbilityRequest> Abilities { get; set; }
        public Guid UserId { get; set; }
    }
}